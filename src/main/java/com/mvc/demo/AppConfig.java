package com.mvc.demo;

import com.mvc.demo.mapper.*;
import com.mvc.demo.properties.StorageProperties;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class AppConfig {

    @Bean
    public StorageProperties getStorageProperties(){
        return new StorageProperties();
    }

    @Bean
    public UserMapper getUserMapper() {
        return Mappers.getMapper(UserMapper.class);
    }

    @Bean
    public GroupMapper getGroupMapper() {
        return Mappers.getMapper(GroupMapper.class);
    }

    @Bean
    public ServerFileMapper getServerFileMapper(){
        return Mappers.getMapper(ServerFileMapper.class);
    }

    @Bean
    public FileVersionMapper getFileVersionMapper(){
        return Mappers.getMapper(FileVersionMapper.class);
    }

//    @Bean
//    public CustomInfoUserMapper getCustomInfoUserMapper(){
//        return Mappers.getMapper(CustomInfoUserMapper.class);
//    }

    @Value("${spring.datasource.driver-class-name}")
    private String driver;
    @Value("${spring.datasource.password}")
    private String password;
    @Value("${spring.datasource.url}")
    private String url;

    @Bean
    public DataSource getDataSource() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName(driver);
        dataSourceBuilder.url(url);
        dataSourceBuilder.username("postgres");
        dataSourceBuilder.password(password);
        return dataSourceBuilder.build();
    }

}
