package com.mvc.demo.handlers;

import com.mvc.demo.exceptions.ApiError;
import com.mvc.demo.exceptions.BadFileException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class BadFileExceptionHandler {

    @ExceptionHandler(BadFileException.class)
    public ResponseEntity<ApiError> noSuchType(BadFileException e){
        return new ResponseEntity<>(new ApiError(e.getMessage()), HttpStatus.CONFLICT);
    }

}
