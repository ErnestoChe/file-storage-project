package com.mvc.demo.exceptions;

public class NoSuchEntityException extends RuntimeException {

    public static final String cliche = "No %s named %s";

    public NoSuchEntityException(String entityType, String entityName) {
        super(String.format(cliche, entityType, entityName));
    }

}
