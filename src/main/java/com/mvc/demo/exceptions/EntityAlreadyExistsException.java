package com.mvc.demo.exceptions;

public class EntityAlreadyExistsException  extends RuntimeException{

    public static final String cliche = "%s with name %s already exists";

    public EntityAlreadyExistsException(String entityName, String objectName) {
        super(String.format(cliche, entityName, objectName));
    }
}
