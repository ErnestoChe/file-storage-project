package com.mvc.demo.exceptions;

public class BadFileException extends RuntimeException{

    public static final String cliche = "Provided file is empty or broken";

    public BadFileException(){
        super(cliche);
    }


}
