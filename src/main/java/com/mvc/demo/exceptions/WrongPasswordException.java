package com.mvc.demo.exceptions;

import org.aspectj.apache.bcel.classfile.annotation.RuntimeTypeAnnos;

public class WrongPasswordException extends RuntimeException {

    public static final String cliche = "wrong password";

    public WrongPasswordException() {
        super(cliche);
    }
}
