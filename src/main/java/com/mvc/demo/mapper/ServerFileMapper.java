package com.mvc.demo.mapper;

import com.mvc.demo.dto.ServerFileDto;
import com.mvc.demo.entities.ServerFile;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface ServerFileMapper {

    ServerFileDto toDto(ServerFile serverFile);

    List<ServerFileDto> listToDto(List<ServerFile> serverFiles);

}
