package com.mvc.demo.mapper;

import com.mvc.demo.dto.UserDto;
import com.mvc.demo.entities.User;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {

    UserDto toDto(User user);

    List<UserDto> listToDto(List<User> users);
}
