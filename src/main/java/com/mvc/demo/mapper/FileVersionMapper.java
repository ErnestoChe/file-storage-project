package com.mvc.demo.mapper;

import com.mvc.demo.dto.FileVersionDto;
import com.mvc.demo.entities.FileVersion;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface FileVersionMapper {

    FileVersionDto toDto(FileVersion serverFile);

    List<FileVersionDto> listToDto(List<FileVersion> fileVersions);

}
