package com.mvc.demo.mapper;

import com.mvc.demo.dto.GroupDto;
import com.mvc.demo.entities.Group;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface GroupMapper {

    GroupDto toDto(Group group);

    List<GroupDto> listToDto(List<Group> groups);
}
