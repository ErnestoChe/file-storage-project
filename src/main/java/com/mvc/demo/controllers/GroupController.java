package com.mvc.demo.controllers;

import com.mvc.demo.dto.GroupDto;
import com.mvc.demo.dto.ServerFileDto;
import com.mvc.demo.dto.UserDto;
import com.mvc.demo.entities.Group;
import com.mvc.demo.entities.ServerFile;
import com.mvc.demo.mapper.GroupMapper;
import com.mvc.demo.mapper.ServerFileMapper;
import com.mvc.demo.mapper.UserMapper;
import com.mvc.demo.service.GroupService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/groups")
@AllArgsConstructor
public class GroupController {

    private final GroupService groupService;
    private final GroupMapper groupMapper;
    private final ServerFileMapper fileMapper;
    private final UserMapper userMapper;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public List<GroupDto> groupDtos() {
        List<Group> userGroups = groupService.getUserGroups();
        return groupMapper.listToDto(userGroups);
    }

    @RequestMapping(value = "/group", method = RequestMethod.POST)
    public GroupDto createGroup(@RequestParam("groupName") String groupName) {
        Group group = groupService.createGroup(groupName);
        return groupMapper.toDto(group);
    }

    @RequestMapping(value = "/group", method = RequestMethod.PUT)
    public GroupDto inviteToGroup(@RequestParam("groupName") String groupName,
                                  @RequestParam("username") String username) throws Exception {
        Group group = groupService.inviteUserToGroup(groupName, username);
        return groupMapper.toDto(group);
    }

    @RequestMapping(value = "/user", method = RequestMethod.DELETE)
    public GroupDto removeUserFromGroup(@RequestParam("groupName") String groupName,
                                  @RequestParam("username") String username) throws Exception {
        Group group = groupService.removeUserFromGroup(groupName, username);
        return groupMapper.toDto(group);
    }

    @RequestMapping(value = "/files", method = RequestMethod.GET)
    public List<ServerFileDto> getGroupFiles(@RequestParam("groupName") String groupName) throws Exception {
        List<ServerFile> groupFiles = groupService.getGroupFiles(groupName);
        return fileMapper.listToDto(groupFiles);
    }

    @RequestMapping(value = "/file", method = RequestMethod.PUT)
    public GroupDto addFileToGroup(@RequestParam("groupName") String groupName,
                                              @RequestParam("filename") String filename) throws Exception {
        Group group = groupService.addFileToGroup(groupName, filename);
        return groupMapper.toDto(group);
    }

    @RequestMapping(value = "/group", method = RequestMethod.DELETE)
    public String addFileToGroup(@RequestParam("groupName") String groupName) throws Exception {
        String s = groupService.dissolveGroup(groupName);
        return String.format("group %s deleted", s);
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public List<UserDto> usersInGroup(@RequestParam("groupName") String groupName) throws Exception {
        return userMapper.listToDto(groupService.groupUsers(groupName));
    }

}
