package com.mvc.demo.controllers;

import com.mvc.demo.dto.payload.LoginRequest;
import com.mvc.demo.dto.payload.SignUpRequest;
import com.mvc.demo.service.LoginService;
import com.mvc.demo.service.SignUpService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import static com.mvc.demo.properties.UriProperties.LOGIN;
import static com.mvc.demo.properties.UriProperties.SIGNUP;

@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor
public class
LoginController{

    private final LoginService loginService;
    private final SignUpService signUpService;

    @RequestMapping(value = LOGIN, method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody LoginRequest loginRequest) {
        return loginService.login(loginRequest);
    }

    @RequestMapping(value = SIGNUP, method = RequestMethod.POST)
    public ResponseEntity registerUser(@RequestBody SignUpRequest signUpRequest) {
        return signUpService.signUp(signUpRequest);
    }
}
