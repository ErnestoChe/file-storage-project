package com.mvc.demo.controllers;

import com.mvc.demo.entities.User;
import com.mvc.demo.service.CurrentUserService;
import com.mvc.demo.service.InformationService;
import com.mvc.demo.utils.UserInfoReport;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class UserController {

    private final CurrentUserService currentUserService;
    private final InformationService informationService;

    @RequestMapping(value = "/main", method = RequestMethod.GET)
    public UserInfoReport signUp() {
        User currentUser = currentUserService.getCurrentUser();
        return informationService.getUserInformation(currentUser);
    }
}
