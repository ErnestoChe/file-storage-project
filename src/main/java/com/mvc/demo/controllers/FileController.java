package com.mvc.demo.controllers;


import com.mvc.demo.dto.FileVersionDto;
import com.mvc.demo.dto.ServerFileDto;
import com.mvc.demo.entities.ServerFile;
import com.mvc.demo.mapper.FileVersionMapper;
import com.mvc.demo.mapper.ServerFileMapper;
import com.mvc.demo.service.CurrentUserService;
import com.mvc.demo.service.FileStorageService;
import com.mvc.demo.service.FileVersionService;
import com.mvc.demo.utils.UploadReport;
import lombok.AllArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

import static com.mvc.demo.properties.UriProperties.*;

@RestController
@RequestMapping("/api/files")
@AllArgsConstructor
public class FileController {

    private final FileStorageService fileStorageService;
    private final ServerFileMapper serverFileMapper;
    private final CurrentUserService currentUserService;
    private final FileVersionService fileVersionService;
    private final FileVersionMapper fileVersionMapper;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public List<ServerFileDto> files() {
        List<ServerFile> files = fileStorageService
                .newFilesByUser(currentUserService.getCurrentUser());
        return serverFileMapper.listToDto(files);
    }

    @RequestMapping(value = "/file", method = RequestMethod.POST)
    public UploadReport uploadFile  (@RequestParam("file") MultipartFile file) throws IOException {
        return fileStorageService.save(file);
    }

    @RequestMapping(value = VERSION, method = RequestMethod.GET)
    public List<FileVersionDto> getVersions(@RequestParam("filename") String filename) throws Exception {
        return fileVersionMapper.listToDto(fileVersionService.getVersions(filename));
    }

    @RequestMapping(value = "/file", method = RequestMethod.GET)
    public ResponseEntity<Resource> downloadFile(
            @RequestParam("filename") String filename,
            @RequestParam(value = "version", required = false) Integer version) throws Exception {
        Resource serve = fileStorageService.serve(filename, version);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + serve.getFilename() + "\"")
                .body(serve);
    }

    @RequestMapping(value = "/file", method = RequestMethod.DELETE)
    public ResponseEntity deleteFile(@RequestParam("filename") String filename) {
        fileStorageService.delete(filename);
        return ResponseEntity.ok("File successfully deleted");
    }
}
