package com.mvc.demo.repository;

import com.mvc.demo.entities.FileVersion;
import com.mvc.demo.entities.ServerFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FileVersionRepository extends JpaRepository<FileVersion, Long> {

    List<FileVersion> findFileVersionByServerFile(ServerFile serverFile);

}
