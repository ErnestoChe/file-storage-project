package com.mvc.demo.repository;


import com.mvc.demo.entities.ServerFile;
import com.mvc.demo.entities.User;
import com.mvc.demo.entities.enums.State;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ServerFileRepository extends JpaRepository<ServerFile, Long> {

    List<ServerFile> findByUser(User user);

    Optional<ServerFile> findByName(String name);

    @Modifying
    @Query("update ServerFile sf set sf.version = :version WHERE sf.id = :fileId")
    void updateVersion(@Param("fileId") Long id, @Param("version") Integer version);

    @Modifying
    @Query("update ServerFile sf set sf.size = :size WHERE sf.id = :fileId")
    void updateSize(@Param("fileId") Long id, @Param("size") Long version);

}
