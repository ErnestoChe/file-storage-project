package com.mvc.demo.service;

import com.mvc.demo.dto.FileVersionDto;
import com.mvc.demo.entities.ServerFile;
import com.mvc.demo.entities.User;
import com.mvc.demo.utils.UploadReport;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface FileStorageService {

    UploadReport save(MultipartFile file) throws IOException;

    Resource serve(String filename, Integer version) throws Exception;

    void delete(String filename);

    List<ServerFile> newFilesByUser(User user);



}
