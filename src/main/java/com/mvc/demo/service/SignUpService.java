package com.mvc.demo.service;

import com.mvc.demo.dto.UserDto;
import com.mvc.demo.dto.payload.SignUpRequest;
import com.mvc.demo.entities.form.UserForm;
import org.springframework.http.ResponseEntity;

public interface SignUpService {

    ResponseEntity signUp(SignUpRequest signUpRequest);

}
