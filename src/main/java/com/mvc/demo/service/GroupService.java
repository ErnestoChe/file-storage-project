package com.mvc.demo.service;

import com.mvc.demo.entities.Group;
import com.mvc.demo.entities.ServerFile;
import com.mvc.demo.entities.User;

import java.util.List;

public interface GroupService {

    List<Group> getUserGroups();

    Group createGroup(String name);

    Group inviteUserToGroup(String groupName, String username) throws Exception;

    Group removeUserFromGroup(String groupName, String username) throws Exception;

    List<ServerFile> getGroupFiles(String groupName) throws Exception;

    Group addFileToGroup(String groupName, String fileName) throws Exception;

    String dissolveGroup(String groupName) throws Exception;

    List<User> groupUsers(String groupName) throws Exception;

}
