package com.mvc.demo.service.impl;

import com.mvc.demo.entities.FileVersion;
import com.mvc.demo.entities.Group;
import com.mvc.demo.entities.ServerFile;
import com.mvc.demo.entities.User;
import com.mvc.demo.exceptions.NoSuchEntityException;
import com.mvc.demo.properties.StorageProperties;
import com.mvc.demo.repository.FileVersionRepository;
import com.mvc.demo.repository.ServerFileRepository;
import com.mvc.demo.service.CurrentUserService;
import com.mvc.demo.service.FileVersionService;
import com.mvc.demo.utils.UploadReport;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class FileVersionServiceImpl implements FileVersionService {

    private final StorageProperties storageProperties;
    private final FileVersionRepository fileVersionRepository;
    private final ServerFileRepository serverFileRepository;
    private final CurrentUserService currentUserService;
    private final String DIR_SEPARATOR = "\\";

    @PostConstruct
    public void init() {
        try {
            if (!Files.exists(storageProperties.getNewLocation())) {
                Files.createDirectory(storageProperties.getNewLocation());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public UploadReport saveVersion(User currentUser,
                                    MultipartFile file,
                                    Integer version,
                                    ServerFile parentFile) throws IOException {
        String versionName = file.getOriginalFilename() + "(" + version + ")";

        String uuidAddress = UUID.randomUUID().toString();

        FileVersion toSave = FileVersion.builder()
                .size(file.getSize())
                .serverFile(parentFile)
                .address(uuidAddress)
                .name(versionName)
                .build();

        Path path = Paths.get(storageProperties.getNewLocation() + DIR_SEPARATOR + uuidAddress);

        Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);

        fileVersionRepository.save(toSave);

        return new UploadReport(file.getOriginalFilename(), file.getSize(), file.getContentType(), version);
    }

    @Override
    public Path serveVersion(String filename, Integer version) throws Exception {
        ServerFile fileByFilename = getFileByFilename(filename);

        List<FileVersion> versionsList = fileVersionRepository.findFileVersionByServerFile(fileByFilename);
        FileVersion fileVersion;
        if(version <= versionsList.size()){
            fileVersion = versionsList.get(version);
        }else{
            fileVersion = versionsList.get(versionsList.size() - 1);
        }

        String address = fileVersion.getAddress();

        return Paths.get(storageProperties.getNewLocation() + DIR_SEPARATOR + address);
    }

    @Override
    public List<FileVersion> getVersions(String filename) throws Exception {
        ServerFile fileByFilename = getFileByFilename(filename);
        return fileVersionRepository.findFileVersionByServerFile(fileByFilename);
    }

    @Override
    public void deleteVersions(ServerFile file) {
        List<FileVersion> fileVersionByServerFile = fileVersionRepository.findFileVersionByServerFile(file);
        fileVersionByServerFile.forEach(version -> {
            try {
                Files.deleteIfExists(
                        Paths.get(storageProperties.getNewLocation() + DIR_SEPARATOR + version.getAddress()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        fileVersionRepository.deleteInBatch(fileVersionByServerFile);

    }

    private ServerFile getFileByFilename(String filename) throws Exception {
        Optional<ServerFile> byName = serverFileRepository.findByName(filename);

        if(byName.isEmpty()){
            throw new NoSuchEntityException("File", filename);
        }
        ServerFile serverFile = byName.get();
        if(isFileAccessible(serverFile)){
            return serverFile;
        }else{
            System.out.println("you have no access to file");
            throw new Exception();
        }
    }

    private boolean isFileAccessible(ServerFile serverFile){
        User currentUser = currentUserService.getCurrentUser();
        boolean inUser = currentUser.getServerFiles().contains(serverFile);
        if (inUser) return true;
        for (Group group : currentUser.getGroups()) {
            if(group.getFiles().contains(serverFile)){
                return true;
            }
        }
        return false;
    }
}
