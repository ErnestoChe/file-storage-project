package com.mvc.demo.service.impl;

import com.mvc.demo.entities.User;
import com.mvc.demo.exceptions.UserNotFoundException;
import com.mvc.demo.repository.UserRepository;
import com.mvc.demo.security.details.UserDetailsImpl;
import com.mvc.demo.service.CurrentUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class CurrentUserServiceImpl implements CurrentUserService {

    private final UserRepository userRepository;

    @Override
    public User getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = ((UserDetailsImpl) auth.getPrincipal()).getUsername();
        return userRepository.findByUsername(username).orElseThrow(() -> new UserNotFoundException(username));
    }
}
