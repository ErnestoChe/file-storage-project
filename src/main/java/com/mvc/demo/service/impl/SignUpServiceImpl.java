package com.mvc.demo.service.impl;

import com.mvc.demo.dto.payload.SignUpRequest;
import com.mvc.demo.dto.response.MessageResponse;
import com.mvc.demo.entities.Role;
import com.mvc.demo.entities.User;
import com.mvc.demo.entities.enums.ERole;
import com.mvc.demo.repository.RoleRepository;
import com.mvc.demo.repository.UserRepository;
import com.mvc.demo.service.SignUpService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.HashSet;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class SignUpServiceImpl implements SignUpService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder encoder;

    @Override
    public ResponseEntity<?> signUp(SignUpRequest signUpRequest) {
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }
        if (userRepository.existsByLogin(signUpRequest.getLogin())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Login is already in use!"));
        }
        Set<String> strRoles = signUpRequest.getRole();
        Set<Role> roles = new HashSet<>();
        if (strRoles == null || strRoles.isEmpty()) {
            Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        } else {
            strRoles.forEach(roleFromPayload -> {
                Role userRole = roleRepository.findByName(ERole.valueOf(roleFromPayload))
                        .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                roles.add(userRole);
            });
        }
        User user = User.builder()
                .username(signUpRequest.getUsername())
                .login(signUpRequest.getLogin())
                .password(encoder.encode(signUpRequest.getPassword()))
                .roles(roles).build();
        userRepository.save(user);
        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }
}
