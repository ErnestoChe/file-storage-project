package com.mvc.demo.service.impl;

import com.mvc.demo.entities.Group;
import com.mvc.demo.entities.ServerFile;
import com.mvc.demo.entities.User;
import com.mvc.demo.exceptions.EntityAlreadyExistsException;
import com.mvc.demo.exceptions.NoSuchEntityException;
import com.mvc.demo.repository.GroupRepository;
import com.mvc.demo.repository.ServerFileRepository;
import com.mvc.demo.repository.UserRepository;
import com.mvc.demo.service.CurrentUserService;
import com.mvc.demo.service.GroupService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class GroupServiceImpl implements GroupService {

    private final GroupRepository groupRepository;
    private final CurrentUserService currentUserService;
    private final UserRepository userRepository;
    private final ServerFileRepository fileRepository;

    @Override
    public List<Group> getUserGroups() {
        User currentUser = currentUserService.getCurrentUser();
        return currentUser.getGroups();
    }

    @Override
    public Group createGroup(String name) {
        if(!groupRepository.existsByName(name)){
            Group newGroup = Group.builder()
                    .name(name)
                    .users(Collections.singletonList(currentUserService.getCurrentUser()))
                    .files(new ArrayList<>())
                    .build();
            return groupRepository.save(newGroup);
        }else{
            throw new EntityAlreadyExistsException("Group", name);
        }
    }

    public List<User> groupUsers(String groupName) throws Exception {
        Group groupByName = getGroupByName(groupName);
        return groupByName.getUsers();
    }

    @Override
    public Group inviteUserToGroup(String groupName, String username) throws Exception {
        Group groupByName = getGroupByName(groupName);
        User userByUsername = getUserByUsername(username);
        if(groupByName.getUsers().contains(userByUsername)){
            //TODO custom exception - user already in group
            System.out.println("user with username " + username + " already in group");
            return null;
        }else{
            groupByName.getUsers().add(userByUsername);
            userByUsername.getGroups().add(groupByName);
            return groupRepository.save(groupByName);
        }
    }

    @Override
    public Group removeUserFromGroup(String groupName, String username) throws Exception {
        Group groupByName = getGroupByName(groupName);
        User userByUsername = getUserByUsername(username);
        List<User> users = groupByName.getUsers();
        if(userByUsername.getUsername().equals(currentUserService.getCurrentUser().getUsername())){
            if(users.size() == 1){
                System.out.println("you cannot delete yourself from group if you are the only member");
            }
        }else if(users.contains(userByUsername)){
            userByUsername.getGroups().remove(groupByName);
            groupByName.getUsers().remove(userByUsername);
            return groupRepository.save(groupByName);
        }else{
            //TODO user not in group
            System.out.println("user with username " + username + " not in group");
        }
        return null;
    }

    @Override
    public String dissolveGroup(String groupName) throws Exception {
        Group groupByName = getGroupByName(groupName);
        if(currentUserService.getCurrentUser().getGroups().contains(groupByName)){
            groupByName.getUsers().clear();
            groupByName.getFiles().clear();
            Group save = groupRepository.save(groupByName);
            groupRepository.delete(save);
        }else{
            System.out.println("you have no access to group");
        }
        return "you have no access to group";
    }

    @Override
    public List<ServerFile> getGroupFiles(String groupName) throws Exception {
        Group groupByName = getGroupByName(groupName);
        return groupByName.getFiles();
    }

    @Override
    public Group addFileToGroup(String groupName, String fileName) throws Exception {
        Group groupByName = getGroupByName(groupName);
        ServerFile fileByFilename = getFileByFilename(fileName);
        if(currentUserService.getCurrentUser().getServerFiles().contains(fileByFilename)){
            groupByName.getFiles().add(fileByFilename);
            return groupRepository.save(groupByName);
        }else{
            System.out.println("you have no access to this file");
            throw new NoSuchEntityException("File", fileName);
        }
    }

    private Group getGroupByName(String groupName) throws Exception {
        Optional<Group> byName = groupRepository.findByName(groupName);
        if(byName.isEmpty()){
            throw new NoSuchEntityException("Group", groupName);
        }
        return byName.get();
    }

    private User getUserByUsername(String username) throws Exception {
        Optional<User> byName = userRepository.findByUsername(username);
        if(byName.isEmpty()){
            throw new NoSuchEntityException("User", username);
        }
        return byName.get();
    }

    private ServerFile getFileByFilename(String filename) throws Exception {
        Optional<ServerFile> byName = fileRepository.findByName(filename);
        if(byName.isEmpty()){
            throw new NoSuchEntityException("File", filename);
        }
        return byName.get();
    }
}
