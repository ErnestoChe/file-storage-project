package com.mvc.demo.service.impl;

import com.mvc.demo.entities.ServerFile;
import com.mvc.demo.entities.User;
import com.mvc.demo.exceptions.BadFileException;
import com.mvc.demo.exceptions.NoSuchEntityException;
import com.mvc.demo.repository.ServerFileRepository;
import com.mvc.demo.service.CurrentUserService;
import com.mvc.demo.service.FileStorageService;
import com.mvc.demo.service.FileVersionService;
import com.mvc.demo.utils.UploadReport;
import lombok.AllArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class FileStorageServiceImpl implements FileStorageService {

    private final CurrentUserService currentUserService;
    private final ServerFileRepository serverFileRepository;
    private final FileVersionService fileVersionService;

    public List<ServerFile> newFilesByUser(User user) {
        return serverFileRepository.findByUser(user);
    }

    @Transactional
    @Override
    public UploadReport save(MultipartFile file) throws IOException {
        if (file == null || file.isEmpty()) {
            throw new BadFileException();
        }
        User currentUser = currentUserService.getCurrentUser();
        Integer version = 1;

        ServerFile serverFile = ServerFile.builder()
                .user(currentUser)
                .name(file.getOriginalFilename())
                .size(file.getSize())
                .version(version)
                .build();

        Optional<ServerFile> byName = serverFileRepository.findByName(file.getOriginalFilename());

        if(byName.isEmpty()){
            serverFileRepository.save(serverFile);
        }else{
            ServerFile file1 = byName.get();
            serverFileRepository.updateVersion(file1.getId(), file1.getVersion() + 1);
            serverFileRepository.updateSize(file1.getId(), file1.getSize() + file.getSize());
            version = file1.getVersion() + 1;
            serverFile = file1;
        }

        return fileVersionService.saveVersion(currentUser, file, version, serverFile);
    }

    @Override
    public Resource serve(String filename, Integer version) throws Exception {
        Path filePath = fileVersionService.serveVersion(filename, version);

        if (!isFileExists(filePath)) {
            throw new NoSuchEntityException("File", filename);
        }
        UrlResource urlResource = null;
        try {
            urlResource = new UrlResource(filePath.toUri());
        } catch (MalformedURLException e) {
            throw new NoSuchEntityException("File", filename);
        }
        return urlResource;
    }

    @Override
    public void delete(String filename) {
        Optional<ServerFile> byName = serverFileRepository.findByName(filename);

        if(byName.isPresent()){
            ServerFile serverFile = byName.get();
            fileVersionService.deleteVersions(serverFile);
            serverFileRepository.deleteById(serverFile.getId());
        }else{
            throw new NoSuchEntityException("File", filename);
        }

    }

    private boolean isFileExists(Path pathToFile) {
        return Files.exists(pathToFile) && !Files.isDirectory(pathToFile);
    }
}
