package com.mvc.demo.service.impl;

import com.mvc.demo.entities.ServerFile;
import com.mvc.demo.entities.User;
import com.mvc.demo.mapper.ServerFileMapper;
import com.mvc.demo.service.InformationService;
import com.mvc.demo.utils.UserInfoReport;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class InformationServiceImpl implements InformationService {

    private final ServerFileMapper fileMapper;

    @Value("${drive.values.freespace}")
    private Float freespace;

    public static final String MESSAGE_CLICHE = "Based on your previous file uploads, ";

    @Override
    public UserInfoReport getUserInformation(User user) {

        return new UserInfoReport(
                user.getLogin(),
                user.getUsername(),
                fileMapper.listToDto(user.getServerFiles()),
                getFreePercent(user.getServerFiles()) + "%",
                additionalInfo(user.getServerFiles()));
    }

    private float getFreePercent(List<ServerFile> serverFiles){
        float occupiedSpace = serverFiles.stream().mapToLong(ServerFile::getSize).sum();
        return (100 - occupiedSpace * 100/ freespace);
    }

    private String additionalInfo(List<ServerFile> serverFiles){
        String textInfo = "";

        float freePercent = getFreePercent(serverFiles);
        float occupiedPercent = 100 - freePercent;
        int availableForUpload = (int) ((freePercent - occupiedPercent) / serverFiles.size());

        if(availableForUpload == 0){
            textInfo = MESSAGE_CLICHE + " soon you won't be able to upload new files";
        }else{
            textInfo = String.format(MESSAGE_CLICHE + " you can upload %s more files",
                    availableForUpload);
        }

        return textInfo;
    }
}
