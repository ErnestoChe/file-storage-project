package com.mvc.demo.service;

import com.mvc.demo.entities.FileVersion;
import com.mvc.demo.entities.ServerFile;
import com.mvc.demo.entities.User;
import com.mvc.demo.utils.UploadReport;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public interface FileVersionService {

    UploadReport saveVersion(User currentUser, MultipartFile file, Integer version, ServerFile parentFile) throws IOException;

    Path serveVersion(String filename, Integer version) throws Exception;

    List<FileVersion> getVersions(String filename) throws Exception;

    void deleteVersions(ServerFile file);

}
