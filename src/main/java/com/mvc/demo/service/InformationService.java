package com.mvc.demo.service;

import com.mvc.demo.entities.User;
import com.mvc.demo.utils.UserInfoReport;

public interface InformationService {

    UserInfoReport getUserInformation(User user);

}
