package com.mvc.demo.service;


import com.mvc.demo.dto.payload.LoginRequest;
import org.springframework.http.ResponseEntity;

public interface LoginService {

    ResponseEntity login(LoginRequest loginRequest);

}