package com.mvc.demo.service;

import com.mvc.demo.entities.User;

public interface CurrentUserService {

    User getCurrentUser();

}
