package com.mvc.demo.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ServerFileDto {

    private String name;

    private Integer version;

    private Long size;

}
