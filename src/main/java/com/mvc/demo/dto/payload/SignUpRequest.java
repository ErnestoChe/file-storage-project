package com.mvc.demo.dto.payload;

import lombok.Data;

import java.util.Set;

@Data
public class SignUpRequest {

    private String username;
    private String login;
    private String password;
    private Set<String> role;

}
