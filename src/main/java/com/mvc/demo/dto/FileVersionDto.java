package com.mvc.demo.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FileVersionDto {

    private Integer size;

    private String name;

}
