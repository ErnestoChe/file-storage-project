package com.mvc.demo.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class JwtResponse {

    private String token;
    private final String type = "Bearer";
    private Long id;
    private String username;
    private String login;
    private List<String> roles;

    public JwtResponse(String accessToken, Long id, String username, String login, List<String> roles) {
        this.token = accessToken;
        this.id = id;
        this.username = username;
        this.login = login;
        this.roles = roles;
    }

}
