package com.mvc.demo.dto;

import lombok.*;

import java.util.List;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GroupDto {

    private String name;

    private Set<UserDto> users;

    private List<ServerFileDto> files;

}
