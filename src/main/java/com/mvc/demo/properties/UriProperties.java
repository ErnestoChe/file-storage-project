package com.mvc.demo.properties;

public class UriProperties {

    public static final String MAIN = "/main";

    public static final String LOGIN = "/login";

    public static final String SIGNUP = "/signup";

    public static final String FILES = "/files";

    public static final String UPLOAD = "/upload";

    public static final String VERSION = "/version";

    public static final String DOWNLOAD = "/download";
    public static final String DELETE = "/delete";

}
