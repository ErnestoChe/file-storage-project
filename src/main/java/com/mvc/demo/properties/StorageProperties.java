package com.mvc.demo.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.nio.file.Path;

@Component
public class StorageProperties {

    @Value("${storage.new}")
    private String newLocation;

    public Path getNewLocation() {
        return Path.of(newLocation);
    }
}
