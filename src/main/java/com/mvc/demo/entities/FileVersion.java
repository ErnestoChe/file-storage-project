package com.mvc.demo.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FileVersion extends SuperEntity {

    private String address;

    private String name;

    @ManyToOne(cascade =
            {
                    CascadeType.DETACH,
                    CascadeType.MERGE,
                    CascadeType.PERSIST,
                    CascadeType.REFRESH
            }, fetch = FetchType.LAZY)
    @JoinColumn(name = "file_id")
    private ServerFile serverFile;

    private Long size;

}
