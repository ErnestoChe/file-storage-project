package com.mvc.demo.entities.enums;

public enum ERole {

    ROLE_USER, ROLE_ADMIN;
}
