package com.mvc.demo.entities.form;

import com.mvc.demo.controllers.LoginController;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserForm {

    String username;

    String login;

    String password;
}
