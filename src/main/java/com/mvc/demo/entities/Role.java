package com.mvc.demo.entities;

import com.mvc.demo.entities.enums.ERole;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "\"roles\"")
public class Role extends SuperEntity{

    @Enumerated(EnumType.STRING)
    private ERole name;

}
