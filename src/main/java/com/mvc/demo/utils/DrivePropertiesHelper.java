package com.mvc.demo.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class DrivePropertiesHelper {

    @Value("${drive.values.freespace}")
    private Long freespace;

    public Long getFreespace() {
        return freespace;
    }

}
