package com.mvc.demo.utils;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.time.LocalDateTime;

public class UploadReport {

    @JsonProperty("name")
    private final String fileName;

    @JsonProperty("size")
    private final String fileSize;

    @JsonProperty("type")
    private final String mimeType;

    @JsonProperty("time")
    private final LocalDateTime localDateTime;

    @JsonProperty("version")
    private final Integer version;

    public UploadReport(String fileName, long size, String mimeType, Integer version) {
        this.fileName = fileName;
        this.fileSize = humanReadableByteCountBin(size);
        this.mimeType = mimeType;
        this.localDateTime = LocalDateTime.now();
        this.version = version;
    }

    public static String humanReadableByteCountBin(long bytes) {
        long absB = bytes == Long.MIN_VALUE ? Long.MAX_VALUE : Math.abs(bytes);
        if (absB < 1024) {
            return bytes + " B";
        }
        long value = absB;
        CharacterIterator ci = new StringCharacterIterator("KMGTPE");
        for (int i = 40; i >= 0 && absB > 0xfffccccccccccccL >> i; i -= 10) {
            value >>= 10;
            ci.next();
        }
        value *= Long.signum(bytes);
        return String.format("%.1f %ciB", value / 1024.0, ci.current());
    }
}
