package com.mvc.demo.utils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mvc.demo.dto.ServerFileDto;

import java.util.List;

public class UserInfoReport {

    @JsonProperty("login")
    private String login;

    @JsonProperty("username")
    private String username;

    @JsonProperty("files")
    private List<ServerFileDto> serverFiles;

    @JsonProperty("emptySpace")
    private String emptySpace;

    @JsonProperty("additionalInfo")
    private String info;

    public UserInfoReport(String login, String username, List<ServerFileDto> serverFiles, String emptySpace, String info) {
        this.login = login;
        this.username = username;
        this.serverFiles = serverFiles;
        this.emptySpace = emptySpace;
        this.info = info;
    }
}
