FROM postgres:10.5
COPY init.sql /docker-entrypoint-initdb.d/
EXPOSE 5432