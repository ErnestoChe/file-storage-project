# file-storage-project

Кто бы сюда ни залез, пытайтесь разобраться на свой страх и риск
На стадии проектирования этот проект имел больше функциональности


Облако для файлов

Initial start
1) mvn install -DskipTests - собираем jar файл
2) cp target/file-storage-docker.jar src/main/Docker - копируем в папку с докером
3) docker-compose up - билдим докер

Restart after changes
1) mvn clean package -DskipTests
2) cp target/file-storage-docker.jar src/main/Docker
3) cd src/main/docker
4) docker-compose down 
5) docker rmi docker-spring-boot-postgres:latest 
6) docker-compose up

